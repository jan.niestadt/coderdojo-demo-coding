# Opstartcode
# ==========================================

tijd = 0

SCHERMVERHOUDING = 1
LINKS = 0
RECHTS = 0
BOVEN = -500
ONDER = 500

plaatjes = {}

effecten = []

def preload():
  voorbereiden()

def draw():
    # Zet de variable tijd op de huidige tijd
    global tijd, SCHERMVERHOUDING, LINKS, RECHTS
    tijd = millis() / 1000
    SCHERMVERHOUDING = width / height if width > height else 1
    LINKS = -500 * SCHERMVERHOUDING
    RECHTS = 500 * SCHERMVERHOUDING
    grootte = min(width, height) / 1000
    with Verplaats(width / 2, height / 2), Grootte(grootte):
        for effect in effecten:
            effect.teken()
        teken()



# Golven (en andere bewegingen?)
# ==========================================

# Een golf met een bepaalde duur, starttijd, minimum- en maximumwaarde
def _golf(duur = 100, start = 0, min = None, max = None):
    if min == None and max == None:
        min = -1
        max = 1
    elif max == None:
        max = min
        min = -min
    if duur == 0:
        duur = 1 # voorkom delen door nul
    return (max - min) * (sin( (tijd + start) * TWO_PI / duur) + 1) / 2 + min


# Kleuren
# ==========================================

# Alle kleuren komen voorbij van tijd = 0 tot tijd = 100
def _regenboog(duur = 1, start = 0, doorzichtigheid = 0, sat = 100, val = 100):
  tint = ((tijd + start) * 100 / duur) % 100
  return color(tint, sat, val, 100 - doorzichtigheid)

TRANSPARANT = color(0, 0, 0, 0)

def _knipper(kleur, duur = 1, start = 0):
    tr = color(hue(kleur), saturation(kleur), lightness(kleur), 50)
    return lerpColor(TRANSPARANT, kleur, sin((tijd + start) * TWO_PI / duur) * 0.5 + 0.5)

# Wissel tussen twee kleuren
def _wissel(kleur1, kleur2, duur = 1, start = 0):
    return lerpColor(kleur1, kleur2, sin((tijd + start) * TWO_PI / duur) * 0.5 + 0.5)

# Maak een kleur doorzichtig
def _doorzichtig(kleur, doorzichtigheid = 50):
    tr = color(hue(kleur), saturation(kleur), lightness(kleur), 0)
    return lerpColor(kleur, tr, doorzichtigheid / 100)



def kleur(h = 100, s = 100, b = 100):
    return lambda dt=0: color(h, s, b)

def regenboog(duur = 1, start = 0, doorzichtigheid = 0, sat = 100, val = 100):
    def color(dt=0):
        global tijd
        tijd += dt
        c = _regenboog(duur, start, doorzichtigheid, sat, val)
        tijd -= dt
        return c
    return color
    
def getal(waarde = 0):
    return lambda: waarde

def golf(duur = 100, start = 0, min = None, max = None):
    return lambda: _golf(duur, start, min, max)

def teller(bereik = 100, duur = 100, start = 0):
    return lambda: (tijd * bereik / duur + start) % bereik




# Transformaties
# ==========================================

class Draai(object):
    def __init__(self, hoek):
        self.hoek = hoek
     
    def __enter__(self):
        push()
        rotate(radians(self.hoek))
        return self
 
    def __exit__(self, *args):
        pop()

class Grootte(object):
    def __init__(self, schaal):
        self.schaal = schaal
     
    def __enter__(self):
        push()
        scale(self.schaal)
        return self
 
    def __exit__(self, *args):
        pop()

class Verplaats(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
     
    def __enter__(self):
        push()
        translate(self.x, self.y)
        return self
 
    def __exit__(self, *args):
        pop()