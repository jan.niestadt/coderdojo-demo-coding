

#-----------------------------------------------------------
# Maak het scherm leeg

class Achtergrond:

    def __init(self):
        self.kleur = kleur()

    def teken(self):
        background(self.kleur())


#-----------------------------------------------------------
# Vul het scherm met parallelle lijnen

class Lijnen:

    def __init__(self):
        self.kleur = kleur()
        self.kleurverloop = getal()
        self.hoek = golf(12, 0, 10)
        self.dikte = getal(8)
        self.afstand = getal(40)

    def lijnen(self, stap):
        y = BOVEN * SCHERMVERHOUDING
        dt = 0
        while y <= ONDER * SCHERMVERHOUDING:
            stroke(self.kleur(dt))
            dt += self.kleurverloop() / 100

            # Teken horizontale lijn
            line(LINKS, y, RECHTS, y)

            # Door naar de volgende lijn
            y += stap

    def teken(self):
        # Zorg dat de kleuren veranderen met de tijd
        strokeWeight(self.dikte())
        stroke(self.kleur())

        # Teken horizontale lijnen
        #self.lijnen(self.afstand())
        
        # Teken ze nog een keer, maar iets gedraaid
        with Draai(self.hoek()):
            self.lijnen(self.afstand())

        # IDEE: je kunt ook een mooi moirepatroon maken met twee sets concentrische cirkels!


#-----------------------------------------------------------
# Teken een aantal vormen in een cirkel

def cirkel(grootte):
    circle(0, 0, grootte)

def rechthoek(grootte):
    rect(0, 0, grootte, grootte)

class VormenCirkel:

    def __init__(self):
        self.vorm = cirkel
        self.duur = getal(2)
        self.aantal = getal(12)
        self.grootte = golf(3.5, 0, 2, 4)
        self.vormgrootte = golf(self.duur(), 0, 25, 50)
        self.draaisnelheid = getal(30)
        self.kleur = regenboog(10)

    # Teken een aantal vormen in een cirkel
    def teken(self):
        global tijd
        fill(self.kleur())
        noStroke() # teken geen lijn om de vormen heen, alleen de vulkleur
        with Grootte(self.grootte()):
            i = 0
            n = floor(self.aantal())
            snelheid = self.draaisnelheid()
            d = self.duur()
            while i < n:
                hoek = i * (360 / n) + tijd * snelheid
                with Draai(hoek), Verplaats(100, 0):
                    dt = i / n * d
                    tijd += dt
                    grootte = self.vormgrootte()
                    tijd -= dt
                    self.vorm(grootte)

                # Door naar de volgende cirkel
                i += 1

    # IDEE: gebruik je eigen vorm, bijvoorbeeld een sterretje, hartje of bloem
    #       zie bijv. https://p5js.org/examples/hello-p5-simple-shapes.html

    # IDEE: Hoe kunnen we de vormen verschillende kleuren geven?

    # IDEE: Hoe laten we elke vorm ook nog een kleiner cirkeltje beschrijven?
