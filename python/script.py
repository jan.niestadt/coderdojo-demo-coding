#from setup import *
#from p5 import *
from copy import deepcopy as kopie

# ==========================================

# Wordt 1x aangeroepen aan het begin
def voorbereiden():

    # Laad ons logo-plaatje
    plaatjes['logo'] = loadImage('images/logo.png')


# Lijnenspel op de achtergrond
# (Moiré-patronen)
# ------------------------------------------


# IDEE: Verplaats de tweede set lijnen ook met een golfpatroon, of varieer afstanden tussen de lijnen

# IDEE: Hoe kunnen we de lijnen verschillende kleuren geven?


achtergrond = Achtergrond()
achtergrond.kleur = kleur(0, 0, 0)

lijnen = Lijnen()
lijnen.kleur = regenboog(duur=33, val=25)
lijnen.kleurverloop = getal(20)
lijnen.dikte = golf(2, 0, 4, 8)
lijnen.afstand = golf(3, 0, 30, 40)
#lijnen.hoek = getal(0)
lijnen.hoek = golf(7, 0, 90)
#lijnen.hoek = teller(360, 10) 

lijnen2 = kopie(lijnen)
#lijnen2.hoek = golf(9, 0, 10)
lijnen2.hoek = golf(9, 4.5, 90)
#lijnen2.hoek = teller(360, 10, 90)

cirkels = VormenCirkel()

vierkanten = VormenCirkel()
vierkanten.vorm = rechthoek
vierkanten.aantal = golf(10, 0, 5, 30) #getal(7)
vierkanten.duur = getal(2.5)
vierkanten.draaisnelheid = getal(-20)
vierkanten.grootte = golf(5, 0, 1, 3)
vierkanten.kleur = regenboog(10, 5)
vierkanten.vormgrootte = golf(1, 0, 10, 100)

effecten = [
    achtergrond,
    lijnen,
    lijnen2,
    cirkels,
    vierkanten,
]

# Wordt telkens aangeroepen om het scherm te vullen
def teken():
    pass

    # Maak het canvas leeg
    #background(0)
    #achtergrond.teken()

    #lijnen.teken()
    #lijnen2.teken()

    #teken_vormen()

    lissa_slang(
        lengte=2, 
        d1=5.3, 
        d2=4.9, 
        kleur=_regenboog(0.6))
    # lissa_slang(
    #     lengte=2, 
    #     d1=3, 
    #     d2=2.7, 
    #     kleur=_regenboog(2))

    # plaatje_met_staart(
    #     plaatje='logo', 
    #     teken_functie=teken_logo, 
    #     aantal=3, 
    #     tijdstap=0.1, 
    #     d1=7, 
    #     d2=11)
    # plaatje_met_staart(
    #     plaatje='logo', 
    #     teken_functie=teken_logo, 
    #     aantal=3, 
    #     tijdstap=0.1, 
    #     d1=5, 
    #     d2=9, 
    #     start=3)
    
    # schrijf_tekst()


# (deel van) lissajous patroon
# ------------------------------------------

def lissa_slang(lengte = 1, start = 0, d1 = 4.3, d2 = 4, kleur = "white"):
    global tijd # nodig omdat we tijd aanpassen

    # Zet kleur en dikte van de lijn
    stroke(kleur)
    strokeWeight(50);

    echteTijd = tijd              # onthoud de tijd

    # Teken een aantal stukjes lijn die samen de slang vormen
    tijdstap = 3 / 100            # hoeveel tijd verstrijkt er per stukje lijn?
    stappen = lengte / tijdstap
    nummer = 1
    x1 = None
    y1 = None
    while nummer <= stappen:
        tijd += tijdstap            # zet de tijd een klein beetje vooruit
        # bepaal het volgende punt
        x2 = _golf(d1, start, 450)
        y2 = _golf(d2, start, 450)
        if x1 != None:
            line(x1, y1, x2, y2)      # teken een stukje lijn
        x1 = x2                     # teken volgende lijnstuk vanaf dit punt
        y1 = y2
        nummer += 1                 # door naar het volgende lijnstuk
  
    tijd = echteTijd   # herstel de tijd die we hebben aangepast zodat de rest niet in de soep loopt...


# Logo zwerft over het scherm, met "schaduwen" erachteraan
# ------------------------------------------

# Roept een aantal keer een tekenfunctie aan
def plaatje_met_staart(plaatje, teken_functie, aantal = 3, tijdstap = 0.05, d1 = 7, d2 = 11, start = 0):
    global tijd
    echteTijd = tijd
    
    nummer = 1
    while nummer <= aantal:

        # Zet de tijd een klein stukje vooruit voor elk plaatje,
        # zodat het op een iets andere plek getekend wordt
        tijd += tijdstap

        # Teken het logo
        teken_functie(plaatje, nummer, aantal, d1, d2, start)

        # Houd nummer bij
        nummer += 1

    tijd = echteTijd # herstel de tijd weer naar de echte waarde


# Teken het logo
def teken_logo(naam, nummer, aantal, d1 = 7, d2 = 11, start = 0):
    # Waar willen we het logo tekenen, en hoe groot?
    grootte = _golf(3, 0, 2, 3)
    x = _golf(d1, start) * 500
    y = _golf(d2, start) * 200 - 100
    with Verplaats(x, y), Grootte(grootte):

        # Zet kleurtint en doorzichtigheid
        doorzichtigheid = (1 - nummer / aantal) * 66
        kleur1 = _regenboog(10, 0, doorzichtigheid)
        kleur2 = _regenboog(10, 5, doorzichtigheid)
        tint(_wissel(kleur1, kleur2, 2))

        # Teken logo
        image(plaatjes[naam], 0, 0)


        # IDEE: kun je ook twee verschillende plaatjes door elkaar laten bewegen?


# Dansende vormen
# ------------------------------------------

# Teken een aantal vormen in een cirkel
def vormen_cirkel(grootte, teken_functie, snelheid = 10, n = 6):
    noStroke() # teken geen lijn om de vormen heen, alleen de vulkleur
    with Grootte(grootte):
        i = 0
        while i < n:
            hoek = i * (360 / n) + tijd * snelheid
            with Draai(hoek), Verplaats(100, 0):
                teken_functie(i, n)

            # Door naar de volgende cirkel
            i += 1

# def teken_rechthoek(nummer, aantal):
#     duur = 2.5                      # hoe snel veranderen de vormen van grootte?
#     start = nummer / aantal * duur  # zorg dat elke vorm een iets andere grootte krijgt
#     grootte = _golf(2.5, start, 1, 2) * 25
#     rect(0, 0, grootte, grootte)

#def teken_cirkel(nummer, aantal):
#    duur = 2                        # hoe snel veranderen de vormen van grootte?
#    start = nummer / aantal * duur  # zorg dat elke vorm een iets andere grootte krijgt
#    grootte = _golf(2, start, 1, 2) * 25
#    circle(0, 0, grootte)

# IDEE: gebruik je eigen vorm, bijvoorbeeld een sterretje, hartje of bloem
#       zie bijv. https://p5js.org/examples/hello-p5-simple-shapes.html

# IDEE: Hoe kunnen we de vormen verschillende kleuren geven?

# IDEE: Hoe laten we elke vorm ook nog een kleiner cirkeltje beschrijven?

#def teken_vormen():
#
#    fill(_regenboog(10))
#    vormen_cirkel(_golf(3.5, 0, 2, 4), teken_cirkel, 30, 12)
#
#    fill(_regenboog(10, 5))
#    vormen_cirkel(_golf(5, 0, 1, 3), teken_rechthoek, -20, 7)


# Teksten in de demo
# ------------------------------------------

tekst = ("                                  "+
    "                               " +
    "Yo yo yo, c0derz in d4 h0us3! " +
    "CoderDojo Leiden is TERUG met een vette nieuwe dojo. " +
    "Alleen de beste coding warez, 100% gegarandeerd educatief. " + 
    "Doe mee als je van coding houdt. L4merz niet welkom! "
    "Groeten aan alle mentoren, deelnemers en ouders. Peace out" +
    "..........................................")

# Index in tekst van de eerste letter die we tonen
scroll_char = 0

# Breedte van huidige karakter
charWidth = -1

# Hoeveel pixels we deze letter naar LINKS hebben verschoven (bepaalt wanneer we de volgende letter tonen)
scrollPos = 0

snelheid = 15

def schrijf_letter(i, x, y):
    fill(_regenboog(1, i / 10))
    text(tekst[i], x, _golf(2, i/30) * 100 + y)

def schrijf_tekst():
    global scroll_char, charWidth, scrollPos

    y = (ONDER - BOVEN) / 4 + _golf(10, 0, -400, 0)
    with Verplaats(0, y):
        fill(0, 80)
        noStroke()
        rect(0, 0, RECHTS - LINKS, 200)

        textSize(80);
        textFont('Coiny'); 

        # IDEE: kies een ander lettertype!
        # Satisfy, Coiny, Anonymous Pro, Major Mono Display, Georgia

        textAlign(LEFT, CENTER);
        fill("white")

        # Teken de tekst
        i = scroll_char
        if charWidth == -1:
            charWidth = textWidth(tekst[i])
        x = -scrollPos + LINKS
        while x < RECHTS:
            schrijf_letter(i, x, 0)
            x += textWidth(tekst[i])
            i += 1
            if i >= len(tekst):
                i = 0

        # Scroll naar LINKS
        scrollPos += snelheid
        if scrollPos >= charWidth:
            # Deze letter is van het scherm af; begin volgende keer bij de volgende letter
            scroll_char += 1
            if scroll_char >= len(tekst):
                scroll_char = 0
            scrollPos -= charWidth
            charWidth = textWidth(tekst[scroll_char])


# ------------------------------------------
# Boilerplate (hoeven de kinderen waarschijnlijk niet aan te passen)



def setup():
    # Wijzig kleurmode
    colorMode(HSB, 100);

    # Teken rechthoeken vanuit het midden
    rectMode(CENTER);
    imageMode(CENTER);
    
    # Maak canvas aan
    createCanvas(window.innerWidth, window.innerHeight)
    # Maak canvas leeg
    background(0)
    

# Wanneer het venster van grootte verandert...
def windowResized(e):
    # ...verander dan ons canvas mee!
    resizeCanvas(window.innerWidth, window.innerHeight)
    background(0)
