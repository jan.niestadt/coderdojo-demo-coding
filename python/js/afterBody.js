function loadScript(url, then) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      then(xhr.responseText);
    }
  };
  xhr.send();
}

document.addEventListener("DOMContentLoaded", function() {
  let setupCode, effectsCode, userCode;
  loadScript('setup.py', (code) => { console.log('setup'); setupCode = code; });
  loadScript('effecten.py', (code) => { console.log('effects'); effectsCode = code; });
  const urlParams = new URLSearchParams(window.location.search);
  const scriptFile = urlParams.get('script') || 'script.py';
  loadScript(scriptFile, (code) => { console.log('script'); userCode = code; });

  function runCode() {
    document.getElementById("sketch-holder").innerHTML = "";

    // from pyp5js
    // window.runSketchCode(setupCode);
    // window.runSketchCode(effectsCode);
    // window.runSketchCode(userCode);
    const code = [
      setupCode,
      effectsCode,
      userCode,
    ].join('\n');
    window.runSketchCode(code);
    window.instance.resizeCanvas(window.innerWidth, window.innerHeight);
  }

  function waitForInstance() {
    if (window.instance && setupCode !== undefined && userCode !== undefined && effectsCode !== undefined) {
        console.log('run');
        runCode();
    } else {
        console.log('waiting...');
        setTimeout(waitForInstance, 250);
    }
  }

  waitForInstance();
});
