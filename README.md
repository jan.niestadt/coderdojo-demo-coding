# CoderDojo: Demo Coding

## Principes

Behalve leuke dingen doen met bouwblokjes gaat deze opdracht primair over het idee van 'nesten'.
Je kunt een paar effecten samen in een groep nemen en die dan over het scherm laten bewegen, of de kleuren aanpassen,
etc. Je kunt ook de demo in fases opdelen; elke fase is een groep effecten die een aantal seconden blijven staan (evt met fadeout door een 'curtain' eroverheen te tekenen, of fadeover naar nieuwe effecten).

Voor de rest moeten we het niet te ingewikkeld/customizable willen maken. We kunnen bij elk effect presets
maken, en wie wil kan alles aanpassen, maar dat is niet nodig om een leuke demo te maken.

Belangrijkste uitdaging is om de syntax zo simpel mogelijk te houden zodat het nesting-aspect duidelijk is,
met de mogelijkheid om geleidelijk door te groeien naar wat complexere aanpassingen.


## INTROTEKST (v1)

Gekraakte computerspelletjes hadden vroeger vaak een cracker demo voordat ze werden gestart. Daarin werd de naam van de cracker getoond, en deden ze de groeten aan andere crackgroups.

Uit deze demo's is de demoscene ontstaan, die nu nog steeds bestaat, maar nu zonder illegale praktijken eromheen. Een demo is een stukje digitale kunst en een manier om te laten zien hoe goed je kunt programmeren.

In deze opdracht ga je zelf een coole demo maken!


## TODO

PRIO 1:
- makkelijker manier om tussen verschillende demo's te kunnen swtichen,
  een "echte" en (tenminste) een "experimentele".
  liefst meerdere zodat je voorbeelden mee kunt leveren waarmee kinderen
  kunnen experimenteren
- meer "prefab" modifiers om het simpel te houden
- normaliseer getallen die operaties gebruiken, zodat een golf zonder
  parameter altijd een goed resultaat geeft, maar je die ook kunt 
  versnellen, vergroten/verkleinen, verschuiven (syntax?)
- lissa: ipv d1 en d2 een ratio en een snelheid? idem met amplitude: vervang door ratio en scale?
- meer modifier effects transparant/trail/(de)activate...?

PRIO 3:
- animated GIF voor de website?
- AUDIO?
- wissel functie voor 2 kleuren
- random_golf functie die telkens wat anders oplevert? (en console.log() 
  de waardes zodat kinderen het vast kunnen leggen als ze er happy mee zijn)
- "random" lissa-pad op basis van een seed, zodat kinderen makkelijker kunnen experimenteren
- ster, hartje, etc.?
- sla effect visibility op in localstorage?

TODO (advanced):
- verschillende fases van de demo te ondersteunen? (activeer/deactiveer groep na X seconden)



IDEE: moire je kunt ook een mooi moirepatroon maken met twee sets concentrische cirkels!
IDEE: moire Verplaats de tweede set lijnen ook met een golfpatroon, of varieer afstanden tussen de lijnen
IDEE: moire Hoe kunnen we de lijnen verschillende kleuren geven?
IDEE: vormen gebruik je eigen vorm, bijvoorbeeld een sterretje, hartje of bloem
      zie bijv. https://p5js.org/examples/hello-p5-simple-shapes.html
IDEE: vormen Hoe kunnen we de vorm verschillende kleuren geven?
IDEE: vormen Hoe laten we elke vorm ook nog een kleiner cirkeltje beschrijven?
IDEE: tekstrol kies een ander lettertype! Satisfy, Coiny, Anonymous Pro, Major Mono Display, Georgia





## Voorbeelden

PC DOS Demoscene mix
https://www.youtube.com/watch?v=O4T7pIs--LA




## Mogelijkheden

- scenes (drawfunctie)
  - automatische fadein/fadeout tussen scenes (teken doorzichtig zwart vlak)

- transformaties
  - scrollende tekst, met verplaatsing/kleur/rotatie per letter en per regel
    (gebruik 2D transforms)

- 3D-achtige effecten met 2D elementen:
  - Parallax?
  - starfield (met of zonder perspectief)
  - eenvoudige 3d objecten..?

- 2D effecten:
  - gradient backgrounds
  - plasma / vuur achtige effecten..? perlin noise?
  - conway's game of life?
  - lissajous figuren / spirograph
  - bouncing balls?
  - reflections
  - multiply filters e.d.?

- muziek? https://www.midijs.net/



## Library-design

Voorbeeld van een demo-script:

```python

plaatje_logo = None

def draw():
  # Zet de variable t op de huidige tijd
  global t, mx, my
  t = tijd()
  mx = width / 2
  my = height / 2

def preload():
  global plaatje_logo
  plaatje_logo = loadImage('images/logo.png')

def logo_scene():
    x = sin(t)
    y = cos(t)
    with push_matrix():
        translate(x, y)
        #rect((20, 20), 40, 40)
        image(plaatje_logo, 0, 0)

def demo():
    scene(logo_scene, 5)


```



## Demo-language?

Hoe zouden we onze voorbeelddemo declaratief kunnen beschrijven?

```
lines($stap):
  voor $y van $boven tot $onder stap $stap:
    line(0, $y, 100, $y))

moire():
  strokeWeight(0.8)
  lines(4)
  rotate(wave(8) / 10)
  lines(4)
```


## Oudere aantekeningen

* coderdojo juli.   (check pyscript.com, pyxel? moirepatronen, lissajous (vereenvoudigd)?)
  https://berinhard.github.io/pyp5js/pyodide
  Works in Replit: https://replit.com/@zwets/PyP5JS-test#index.html
  Ideeen:
  * DONE moire-patronen
  * DONE spirograph / lissajous-figuren
  * spiraal
  * graphing 2d equations https://p5js.org/examples/math-graphing-2d-equations.html
  * parametric equations https://p5js.org/examples/math-parametric-equations.html
  * additive wave https://p5js.org/examples/math-additive-wave.html
  * starfield
  * fireworks
  * verdere inspiratie: https://axltnnr.io/tags/playground/
