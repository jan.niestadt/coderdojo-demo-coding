# Een flitsende demo

Laat zien wat je kunt met je eigen *demo*, een digitaal bewegend kunstwerk!

Klik [hier](https://replit.com/@JanNiestadt/ExampleDemo#demo.js) voor een eenvoudig voorbeeldje. Jij kunt vast iets veel mooiers maken!

Begin bij [het startproject](./1%20-%20startproject.md)!