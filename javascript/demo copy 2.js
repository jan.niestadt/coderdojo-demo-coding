function maakDemo()
{



// Laad ons logo-plaatje
laadPlaatjes('logo3.png')


laag(Achtergrond)
wijzig("kleur", "#8888ff")

/*
laag(Lijnen)
effect(Grootte, 100) //getal.golf2(50, 20))

kopieer_laag()
effect(Draai, getal.wiegen())
*/

laag(Lijnen)
wijzig("kleur", "#6666ee")
wijzig("dikte", 2)
laag(Draai)
wijzig("hoek", getal.teller(1000, 0, 360)) //getal.golf(10, 4))
//wijzig("afstand", getal.golf2(50, 3, 21))


// laag(Slang)
// wijzig("kleur", kleur.regenboog(20))
// wijzig("pad", pad.lissa(19, 23))
// wijzig("lijndikte", 30)
// wijzig("lengte", 30)


laag(Vorm)
wijzig("vorm", vorm.veelhoek(getal.afronden(getal.golf(50, 3, 7))))
//wijzig("vorm", vorm.wissel(3, 5, 20))
wijzig("kleur", kleur.regenboog(80))
wijzig("lijnkleur", "zwart")
wijzig("lijndikte", 3) //getal.golf(10, 0, 12))
//laag(Draai)
//wijzig("hoek", 180)

laag(Grootte)
wijzig("grootte", getal.golf(20, 80, 140))
//effect(Verplaats, pad.stuiter(30, 8, 100))

laag(Vermenigvuldig)
wijzig("afstand", getal.golf(10, 200, 250))
wijzig("aantal", 12) //getal.golf(20, 4, 16))

laag(Draai)
wijzig("hoek", getal.golf(100, 180)) //getal.golf(10, 4))



// laag(Plaatje)
// wijzig("naam", "logo3.png")
// laag(Verplaats)
// wijzig("pad", pad.xy(getal.golf(20), 350))

// laag(Tekstrol)
// wijzig("kleur", "zwart")
// wijzig("lijnkleur", "wit")
// wijzig("lettertype", "boek")
// wijzig("tekst", "Kunst maken met code")
// laag(Draai)
// wijzig("hoek", getal.golf(20, 4))
// laag(Verplaats)
// wijzig("pad", pad.omlaag(300))
// laag(Grootte)
// wijzig("grootte", getal.golf(10, 90, 110))

// laag(Plot)
// wijzig("getal", getal.golf(10))


/*
laag(Tekst)
wijzig("kleur", "zwart")
wijzig("tekst", "Coding is\ncool!")

kopieer_laag()
effect(Verplaats, pad.xy(10, 10))
wijzig("kleur", "wit")

groepeer_lagen(3)
effect(Verplaats, pad.lissa())
effect(Draai, getal.wiegen())
effect(Grootte, getal.onregelmatige_puls())
*/

//laag(Tekstrol)

//wijzig("kleur", 2kleur.regenboog())
//wijzig("tekst", "CoderDojo Leiden")
//wijzig("kleur", "geel")
//wijzig("lettertype", "cartoon")
//effect(Verplaats, pad.stuiter(50, 40))
//effect(Draai, getal.wilde_slinger())
//effect(Grootte, getal.golf2(50, 100, 110))



}
