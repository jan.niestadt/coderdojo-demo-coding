
// ==========================================


// Wordt 1x aangeroepen aan het begin
function maakDemo() {

  // Laad ons logo-plaatje
  laadPlaatjes('logo.png', 'logo2.png')
}




const TEKST = `
  Yo yo yo, c0derz in d4 h0us3!
  CoderDojo Leiden is TERUG met een vette nieuwe dojo.
  Alleen de beste coding warez, 100% gegarandeerd educatief.
  Doe mee als je van coding houdt. L4merz niet welkom!
  Groeten aan alle mentoren, deelnemers en ouders. Peace
  out..........................................
`;




demo = [

  //--------------------------------------------------------------------------------
  // Zwarte achtergrond
  laag(Achtergrond),
  wijzig("kleur", kleur.hsb(0, 0, 0)),

  //--------------------------------------------------------------------------------
  // Moire-patroon met 2 lijnen effecten
  laag(Lijnen),
  
  kopieer_laag(), // maak een kopie van het vorige effect
  effect(Draai, getal.golf(100, 30)), // laat het tweede lijnen effect wiegen
  
  groepeer_lagen(2), // groepeer de 2 lijnen effecten
  effect(Draai, getal.teller(100, 0, 360)), // laat de groep tollen

  //--------------------------------------------------------------------------------
  // VormenCirkel
  laag(VormenCirkel),
  wijzig("vorm", vorm.driehoek()),
  effect(Grootte, getal.golf(35, 2, 4)),
  effect(Draai, getal.teller(300, 360, 0)), // laat de groep tollen
  //effect(Verplaats, pad.lissa(70, 110, 500, 200)),
  //effect(Grootte, getal.golf(30, 2, 3)),

  laag(VormenCirkel),
  wijzig("aantal", 7),

  //--------------------------------------------------------------------------------
  // Slang
  laag(Slang),

  laag(Slang),
  wijzig("pad", pad.lissa(20, 30)),
  effect(Grootte, getal.golf2(100, 0.1, 1, 0)),
  //effect(Grootte, getal.golf(35, 2, 4)),
  //effect(Verplaats, pad.lissa(70, 110, 500, 200)),
  //effect(Grootte, getal.golf(30, 2, 3)),

  //--------------------------------------------------------------------------------
  // Plaatje zwerft over scherm
  laag(Plaatje),
  //effect(Verplaats, pad.lissa(70, 110, 500, 200)),
  effect(Verplaats, pad.xy(getal.zigzag2(20, 500), getal.zigzag2(30, 500))),
  //effect(Draai, getal.teller(100, 0, 360)),
  effect(Draai, getal.wilde_slinger()),
  //effect(Grootte, getal.som(1, getal.deel(weirdFunc, 100))),
  //effect(Grootte, getal.golf2(100, 0.2, 2)),
  effect(Grootte, getal.som(2, g.deel(g.onregelmatige_puls(), 50))),


  //--------------------------------------------------------------------------------
  // Tekstrol
  laag(Tekstrol),
  wijzig("snelheid", 20),
  wijzig("hoogte", g.zigzag2(100, 100)),
  wijzig("kleur", k.regenboog(100)),
  wijzig("tekst", TEKST),
  effect(Verplaats, pad.y(g.golf(5, 200))),
  effect(Draai, g.langzaam_wiegen()),


  //--------------------------------------------------------------------------------
  // Tekstrol
  laag(Tekstrol),
  wijzig("snelheid", 20),
  wijzig("hoogte", g.zigzag2(100, 100)),
  wijzig("kleur", kleur.regenboog(100)),
  wijzig("tekst", TEKST),
  effect(Verplaats, pad.y(-300)),
  effect(Draai, g.langzaam_wiegen()),
  effect(Draai, 180),

  //--------------------------------------------------------------------------------
  // Plot functie
  laag(Plot),
  wijzig("getal", g.wilde_slinger()),

]
